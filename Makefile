EXEC_PATH=build/hello
.PHONY: build
build:
	gcc -o ${EXEC_PATH} $(shell ls src/*.c)
run:
	./${EXEC_PATH}