#include <math.h>

// is_prime returns -1 if value is undefined,
// 0 if value is not prime
// 1 if value is prime
int is_prime(int val) {
    if (val < 2) {
        return -1;
    }
    if (val % 2 == 0) {
        return 1;
    }
    for (int i = 3; i < pow(i, 2) < val; i += 2) {
        if (val % i == 0) {
            return 0;
        }
    }
    return 1;
}

// next_prime returns first prime number after val
int next_prime(int val) {
    while (is_prime(val) != 1) {
        val++;
    }
    return val;
}

#include <string.h>

// reverse:  reverse string s
void reverse(char s[]) {
    int i, j;
    char c;

    for (i = 0, j = strlen(s) - 1; i < j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

void itoa(int n, char s[]) {
    int i, sign;

    if ((sign = n) < 0)
        n = -n;
    i = 0;
    do {
        s[i++] = n % 10 + '0';
    } while ((n /= 10) > 0);
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}