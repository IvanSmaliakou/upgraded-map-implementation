#include "../../model/model.h"

#ifndef HASHTABLE_SERVICE_H
#define HASHTABLE_SERVICE_H

ht_table *new_table_sized(int size);
void add_item(ht_table *table, char *key, char *val);
ht_item *search_item(ht_table *table, const char *key);
void delete_item(ht_table *table, char *key);
void delete_table(ht_table *table);
ht_table *ht_new();
void ht_resize(ht_table *table, int size);
void ht_resize_up(ht_table *ht);
void ht_resize_down(ht_table *ht);

#endif //HASHTABLE_SERVICE_H
