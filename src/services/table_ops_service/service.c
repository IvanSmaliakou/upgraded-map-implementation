#include "service.h"
#include "stdlib.h"
#include "../hash_service/service.c"
#include "utils.c"

#define HT_INITIAL_BASE_SIZE 15


ht_table *new_table_sized(int size) {
    ht_table *ret = malloc(sizeof(ht_table));
    int ext_size = next_prime(size);
    ret->size = ext_size;
    ret->count = 0;
    ret->items = calloc(ext_size, sizeof(ht_item *));
    return ret;
}

void add_item(ht_table *table, char *key, char *val) {
    if ((table->count * 100 / table->size) > 70) {
        ht_resize_up(table);
    }
    if ((table->count * 100 / table->size) < 10) {
        ht_resize_down(table);
    }
    ht_item *ret = malloc(sizeof(ht_item));
    ret->key = key;
    ret->val = val;
    int a = 0;
    int index = calculate_hash(key, table->size, a);
    ht_item *item = table->items[index];
    while (item) {
        index = calculate_hash(key, table->size, a);
        item = table->items[index];
        a++;
    }
    table->items[index] = ret;
    table->count++;
}

ht_item *search_item(ht_table *table, const char *key) {
    int a = 0;
    int index = calculate_hash(key, 6, a);
    ht_item *item = table->items[index];
    while (item) {
        if (strcmp(item->key, key) == 0) {
            return item;
        }
        index = calculate_hash(key, 6, a);
        item = table->items[index];
        a++;
    }
    return NULL;
}

void delete_item(ht_table *table, char *key) {
    int a = 0;
    int index = calculate_hash(key, 6, a);
    ht_item *item = table->items[index];
    while (item) {
        if (strcmp(item->key, key) == 0) {
            free(item);
            return;
        }
        index = calculate_hash(key, 6, a);
        item = table->items[index];
        a++;
    }
}

void delete_table(ht_table *table) {
    for (int i = 0; i < table->size; ++i) {
        ht_item *item = table->items[i];
        if (item != NULL)
            delete_item(table, item->key);

    }
    free(table->items);
    free(table);
}

ht_table *ht_new() {
    return new_table_sized(HT_INITIAL_BASE_SIZE);
}

void ht_resize(ht_table *table, int size) {
    if (size < HT_INITIAL_BASE_SIZE) {
        return;
    }
    ht_table *new_table = new_table_sized(size);
    for (int i = 0; i < size; i++) {
        ht_item *cur = table->items[i];
        if (cur != NULL) {
            add_item(new_table, cur->key, cur->val);
        }
    }
    new_table->size = size;
    delete_table(table);
}

void ht_resize_up(ht_table *ht) {
    const int new_size = ht->size * 2;
    ht_resize(ht, new_size);
}


void ht_resize_down(ht_table *ht) {
    const int new_size = ht->size / 2;
    ht_resize(ht, new_size);
}