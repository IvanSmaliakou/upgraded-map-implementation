#include <string.h>
#include <math.h>

int calculate_hash(const char *str, int num_buckets, int a) {
    long hash = 0;
    const int len_s = strlen(str);
    for (int i = 0; i < len_s; i++) {
        hash += (long)pow(a, len_s - (i+1)) * str[i];
        hash = hash % num_buckets;
    }
    return (int)hash;
}