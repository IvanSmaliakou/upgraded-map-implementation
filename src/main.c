#include <stdio.h>
#include "services/table_ops_service/service.c"

int main() {
    ht_table *table = ht_new();
    for (int i = 0; i < 7; i++) {
        char str_key[16];
        char str_val[16];
        itoa(i, str_key);
        itoa(i+1, str_val);
        add_item(table, str_key, str_val);
    }
    for (int i = 0; i < table->count; i++) {
        if (table->items[i] != NULL) {
            printf("%s:%s\n", table->items[i]->key, table->items[i]->val);
        } else {
            printf("NULL\n");
        }
    }
    return 0;
}
