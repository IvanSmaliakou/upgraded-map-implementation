//
// Created by Ivan Smaliakou on 8/20/20.
//

#ifndef HASHTABLE_MODEL_H
#define HASHTABLE_MODEL_H

typedef char* ERROR;

typedef struct {
    char *key;
    char *val;
} ht_item;

typedef struct {
    ht_item **items;
    int size;
    int count;
} ht_table;
#endif //HASHTABLE_MODEL_H
